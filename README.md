## C3 Parser

This script takes data from data_in and generates automatic mails for the C3 event.
It generates the mail files as ".eml" so you will be able to mass import them to your mail browser.

Place the C3 tickets in **c3_tickets** and the local traffic tickets in **c3_nv_tickets**.
The filesystem database should be called **data_in**.
In **msg_in** you can write your desired mail body. It takes multiple variable parameters you can use for general purpose.
Accepted parameters are: 

`<vorname>`, `<nachname>`, `<email>`, `<hinfahrt>`, `<rueckfahrt>`

Also check the examples in **data_in** and **msg_in**.

#### Usage:

`python parser.py yourmail@example.com "Mail Subject"`

will export all generated mail files to **out** directory.
