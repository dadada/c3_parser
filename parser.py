#!/usr/bin/python
#53c70r
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders
import os
import sys

main_array = []
c3_ticket_array = []
c3_nv_ticket_array = []
counter = 0
tmp_path = r'out'
c3_tickets_path = r'c3_tickets'
c3_nv_tickets_path = r'c3_nv_tickets'


def write_files(vorname, nachname, email, hinfahrt, rueckfahrt):
    global counter
    if counter <= len(main_array):
        out = open("out/" + nachname + "_" + vorname + ".eml", "w")
        body = str(msg).replace("<vorname>", vorname)    \
        .replace("<nachname>", nachname).replace("<email>", email)  \
        .replace("<hinfahrt>", hinfahrt).replace("<rueckfahrt>", rueckfahrt)
        mail = MIMEMultipart()
        mail['From'] = str(sys.argv[1])
        mail['To'] = email
        mail['Subject'] = str(sys.argv[2])
        mail.attach(MIMEText(body, "plain", 'utf8'))
        c3ticket = MIMEBase("application", "octet-stream")
        c3ticket.set_payload(open(str(c3_ticket_array[counter]), "rb").read())
        c3ticket.add_header(
            'Content-Disposition', 'attachment', filename="C3_Ticket.pdf")
        Encoders.encode_base64(c3ticket)
        mail.attach(c3ticket)
        nvticket = MIMEBase("application", "octet-stream")
        nvticket.set_payload(
            open(str(c3_nv_ticket_array[counter]), "rb").read())
        nvticket.add_header(
            'Content-Disposition',
            'attachment',
            filename="Nahverkehrsticket.pdf")
        Encoders.encode_base64(nvticket)
        mail.attach(nvticket)
        out.write(mail.as_string())
        out.close
        counter += 1
        return


if not os.path.exists(tmp_path):
    os.makedirs(tmp_path)

if not os.path.exists(c3_tickets_path):
    os.makedirs(c3_tickets_path)

if not os.path.exists(c3_nv_tickets_path):
    os.makedirs(c3_nv_tickets_path)

for filename in os.listdir("c3_tickets"):
    if filename.endswith(".pdf"):
        c3_ticket_array.append("c3_tickets/" + filename)

for filename in os.listdir("c3_nv_tickets"):
    if filename.endswith(".pdf"):
        c3_nv_ticket_array.append("c3_nv_tickets/" + filename)

c3_ticket_array.sort()
c3_nv_ticket_array.sort()

with open('data_in') as data_in:
    for line in data_in:
        line = line.strip()
        main_array.append(line.split(";"))

if len(main_array) < 1:
    print("No data given.")
    sys.exit()

if len(c3_ticket_array) < len(main_array):
    print("More participants than tickets. Exiting.\n")
    sys.exit()

if len(c3_nv_ticket_array) < len(main_array):
    print("More participants than tickets. Exiting.\n")
    sys.exit()

if len(sys.argv) < 3:
    print("First system argument is your email, second the mail Subject.\n")
    sys.exit()

with open('msg_in') as msg_in:
    msg = msg_in.read()

for x in range(len(main_array)):
    vorname = main_array[x][0]
    nachname = main_array[x][1]
    email = main_array[x][2]
    hinfahrt = main_array[x][3]
    rueckfahrt = main_array[x][4]
    write_files(vorname, nachname, email, hinfahrt, rueckfahrt)
